## oauthmeal

_the incomplete oauth client focused on types, flexibility, few dependencies and learning_

### Who is this crate for

This crate is for me, to scratch an itch to learn about Rust and Oauth. If you are looking for something complete, well tested, benchmarked and written by someone who knows what they are doing then this crate is not for you unless I'm either lucky or underestimating my capabilities. If you for some reason like the crate and want to help out, then I would be thrilled, but I take no offense if you just move on and look for another implementation.

### Goals

 - do not dictate a specific http-client library
 - no fancy complicated backends
 - automatic renewal of tokens
 - no primitive obsession, use strong types
 - do not leak sensitive information in logs unless explicitly specified
 - prefer sharing bearer tokens over security tokens
 - asynchronous support
 - easy to test
 - learning rust and oauth

### Dependencies with reasons

 - `serde`, `serde_json`, `serde_urlencoded`: Oauth needs this
 - `http`: a standard http library seems like a good idea for many reasons
     * testability
     * compatibility
 - `tracing`: curiosity and, it seems like a good idea in an asynchronous context
 
 That should be it for now.
