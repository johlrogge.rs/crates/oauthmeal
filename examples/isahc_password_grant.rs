use isahc;
use oauthmeal::{self, request_token, Request};

fn main() -> Result<(), Box<dyn std::error::Error>> {
        let password_grant_request = Request::Password {
                username: "user".into(),
                password: "password".into(),
        };

        fn do_send(
                request: http::Request<String>,
        ) -> oauthmeal::Result<http::Response<isahc::Body>> {
                println!("request: {:#?}", &request);
                let response = isahc::send(request)
                        .map_err(|e| oauthmeal::error::OauthError::SendError(Box::new(e)))?;
                println!("response: {:#?}", &response);
                Ok(response)
        }

        let token = request_token(
                password_grant_request,
                "https://host/auth/token".parse()?,
                do_send,
        )?;

        println!("{:#?}", token);
        Ok(())
}
