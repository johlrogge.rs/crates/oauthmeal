use serde_urlencoded;
use std::error::Error;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum OauthError {
        #[error("failed to url encode body: {0}")]
        UrlEncode(#[from] serde_urlencoded::ser::Error),

        #[error("http error: {0}")]
        Http(#[from] http::Error),

        #[error("io error: {0}")]
        Io(#[from] std::io::Error),

        #[error("send error: {0}")]
        SendError(Box<dyn Error>),

        #[error("json error: {0}")]
        JsonError(#[from] serde_json::Error),
}
