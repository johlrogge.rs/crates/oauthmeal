pub mod error;
use http;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};
use std::io::Read;
use std::time::Duration;

#[derive(Serialize, Deserialize)]
pub struct Username<'a>(&'a str);

#[derive(Serialize, Deserialize)]
pub struct Password<'a>(&'a str);

/// A Request
#[derive(Serialize)]
#[serde(tag = "grant_type", rename_all = "lowercase")]
pub enum Request<'a> {
        /// Request a token with a password grant
        Password {
                username: Username<'a>,
                password: Password<'a>,
        },
}

/// A token
#[derive(Debug, Deserialize, Serialize)]
pub struct Token(String);

/// A response
#[derive(Serialize, Deserialize, Debug)]
pub struct Response {
        access_token: Token,
        token_type: String,
        #[serde(deserialize_with = "deserialize_seconds_as_duration")]
        expires_in: Option<Duration>,
        refresh_token: Option<String>,
        scope: Option<String>,
}

/// serialize into duration
fn deserialize_seconds_as_duration<'de, D>(
        deserializer: D,
) -> std::result::Result<Option<Duration>, D::Error>
where
        D: serde::Deserializer<'de>,
{
        let seconds = Option::<u64>::deserialize(deserializer)?;
        Ok(seconds.map(|s| Duration::from_secs(s)))
}

/// Create a username from a &str
impl<'a> From<&'a str> for Username<'a> {
        fn from(s: &'a str) -> Self {
                Username(s)
        }
}

/// Create a password from a &str
impl<'a> From<&'a str> for Password<'a> {
        fn from(s: &'a str) -> Self {
                Password(s)
        }
}

/// Try to convert a Request to a http::Request
impl<'a> TryFrom<Request<'a>> for http::Request<String> {
        type Error = error::OauthError;
        fn try_from(oauth_request: Request<'a>) -> Result<Self> {
                let result = match oauth_request {
                        Request::Password { .. } => http::Request::builder()
                                .method("POST")
                                .header("accept", "application/json")
                                .header("content-type", "application/x-www-form-urlencoded")
                                .body(serde_urlencoded::to_string(oauth_request)?)?,
                };
                Result::Ok(result)
        }
}

pub fn request_token<S, R>(
        oauth_request: Request,
        auth_uri: http::Uri,
        mut sender: S,
) -> Result<Response>
where
        S: FnMut(http::Request<String>) -> Result<http::Response<R>>,
        R: Read,
{
        let mut http_request: http::Request<String> = oauth_request.try_into()?;
        *http_request.uri_mut() = auth_uri;
        let mut response = sender(http_request)?;
        let body = response.body_mut();
        Ok(serde_json::from_reader(body)?)
}

pub type Result<T> = std::result::Result<T, error::OauthError>;

#[cfg(test)]
mod test {
        use super::Request;
        use http;
        use std::convert::TryInto;
        #[test]
        fn serialize_password_grant_request() {
                let request = Request::Password {
                        username: "999999".into(),
                        password: "xhyHUWEEN".into(),
                };

                let request = serde_urlencoded::to_string(request).unwrap();
                assert_eq!(
                        request,
                        "grant_type=password&username=999999&password=xhyHUWEEN"
                );
        }

        #[test]
        fn password_grant_request_into_html() {
                let request = Request::Password {
                        username: "abcdefgh".into(),
                        password: "secret!!!".into(),
                };
                let expected_body = serde_urlencoded::to_string(&request).unwrap();
                let html_request: http::Request<String> = request.try_into().unwrap();
                assert_eq!(
                        html_request.headers().get("accept").unwrap(),
                        "application/json"
                );
                assert_eq!(
                        html_request.headers().get("content-type").unwrap(),
                        "application/x-www-form-urlencoded"
                );
                assert_eq!(html_request.method(), "POST");
                assert_eq!(html_request.body(), &expected_body);
        }
}
